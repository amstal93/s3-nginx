# s3-nginx

Nginx proxy for public S3 bucket.

## Usage

```bash
docker run -e DOMAIN=bucket.s3.amazonaws.com -p 80:80 joshava/s3-nginx
```

### Environment Variables

`DOMAIN`: Domain of the S3 bucket. For example, `bucket.s3.amazonaws.com`.

> Note that items in the bucket must have public read permission.

### Volumes

`/cache`: Used as proxy cache.

### Ports

`80`: Web port.
